module.exports = {
  purge: {
    enabled: process.env.WEBPACK_DEV_SERVER === 'true' && process.argv.join(' ').indexOf('build') !== -1,
    content: [
      "./src/**/*.{html,ts}",
      "./projects/**/*.{html,ts}"
    ]
  },
  darkMode: 'media', //'false' or 'media' or 'class'
  theme: {
    extend:{
      colors:{
        "accent1": {
          "50": "#6686ff",
          "100": "#5c7cf9",
          "200": "#5272ef",
          "300": "#4868e5",
          "400": "#3e5edb",
          "500": "#3454d1",
          "600": "#2a4ac7",
          "700": "#2040bd",
          "800": "#1636b3",
          "900": "#0c2ca9"
        },
        "accent2": {
          "50": "#66fff1",
          "100": "#5cf9e7",
          "200": "#52efdd",
          "300": "#48e5d3",
          "400": "#3edbc9",
          "500": "#34d1bf",
          "600": "#2ac7b5",
          "700": "#20bdab",
          "800": "#16b3a1",
          "900": "#0ca997"
        },
        "accent3": {
          "50": "#ff668d",
          "100": "#f95c83",
          "200": "#ef5279",
          "300": "#e5486f",
          "400": "#db3e65",
          "500": "#d1345b",
          "600": "#c72a51",
          "700": "#bd2047",
          "800": "#b3163d",
          "900": "#a90c33"
        },
        "lightAccent": {
          "50": "#ffffff",
          "100": "#ffffff",
          "200": "#ffffff",
          "300": "#ffffff",
          "400": "#f9f9f9",
          "500": "#efefef",
          "600": "#e5e5e5",
          "700": "#dbdbdb",
          "800": "#d1d1d1",
          "900": "#c7c7c7"
        },
        "darkAccent": {
          "50": "#393939",
          "100": "#2f2f2f",
          "200": "#252525",
          "300": "#1b1b1b",
          "400": "#111111",
          "500": "#070707",
          "600": "#000000",
          "700": "#000000",
          "800": "#000000",
          "900": "#000000"
        },
      },
    },
  },
  variants: {
  },
  plugins: [],
}
